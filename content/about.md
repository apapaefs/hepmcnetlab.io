---
title: "About"
menu:
  main:
    name: "About"
    weight: 2
sidebar: right
widgets: ["sidemenu_about"]
toc: false
---

Monte Carlo event generators are central to high energy particle physics. They
are used by almost all experimental collaborations to plan their experiments and
analyze their data, and by theorists to simulate the complex final states of the
fundamental interactions that may signal new physics.

MCnet was originally an EU-funded Marie Curie Initial Training Network dedicated
to developing and supporting general-purpose Monte Carlo event generators
through the LHC era and beyond. Through three iterations of ITN funding, MCnet
provided training of both particle theorists and experimentalists in MC
event-generation techniques, tools, and applications, particularly through PhD
studentships, funded short-term "residencies", and annual schools.

From 2020, MCnet has graduated to being a community organisation, carrying on much
of the original vision of MCnet in bringing together MC-generator developer teams
and users, coordinating interoperability and best-practice between MC tools, and
continuing to organise and provide workshops and training events, particularly
summer schools.

The community incorporates all the authors of current general-purpose event generators,
and has as its main aims:

- training and connecting the event-generator user base, through annual schools on the physics and techniques of event generators;
- training the next generation of event generator authors through research workshops and other events;
- providing broader training in transferable skills through our research and through secondments to private-sector partners.

These training objectives are being achieved both through dedicated activities
and through our outreach and research activities:

- enhancing the visibility of particle physics in the wider community by specific outreach projects using event generators;
- developing and supporting the new generation of event generators intended for use throughout the LHC data-analysis era and beyond;
- playing a central role in the analysis of LHC data and the discovery of new particles and interactions there; and
- extracting the maximum potential from existing data to constrain the modeling of the data from the LHC and future experiments.
