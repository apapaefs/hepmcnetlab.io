---
title: "About"
lead: "Teams > Durham members"
sidebar: right
widgets: ["sidemenu_about", "team_home_pages"]
toc: false
---

Members at Durham
---------

***

##### Jeppe Andersen (Team Leader)

![](/img/durham/durham_member_andersen.jpg) 

Jeppe R. Andersen is an author of the High Energy Jets formalism and its implemention. His main research interests are in improving the theoretical description of multi-jet observables based on hard perturbative QCD. Recently, the research focus has in particular been on the relevance of multi-jet processes in the analyses of the properties of the Higgs boson.

##### Frank Krauss

![](/img/durham/durham_member_krauss.jpg) 

Frank Krauss is originator and principal author of the Sherpa program.

##### Aidin M.R. Masouminia

![](/img/durham/durham_member_masouminia.jpg) 

Aidin M.R. Masouminia is a postdoc at IPPP, Durham and a member of the Herwig project. His primary research interests include phenomenological QCD, Monte-Carlo event generators, applications of the TMD-PDF at higher-energy colliders and the BSM physics.

##### Silvia Ferrario-Ravasio

![](/img/durham/durham_member_ferrario-ravasio.png) 

Silvia is a post doc researcher working on the Herwig project. Her main research interests involve NLO+PS computations, with a particular focus on the POWHEG BOX and HERWIG7 softwares, heavy quark physics and renormalons.

##### Alan C. Price

![](/img/durham/durham_member_price.jpg) 

Alan is a PhD student working on the SHERPA Project

##### Gavin Bewick

![](/img/durham/durham_member_bewick.jpg)

Gavin is a PhD student working on Herwig.

Other MCnet-related staff

##### James Black

![](/img/durham/durham_member_black.jpg)

James is a PhD student working on the High Energy Jets project.

##### Marian Heil

![](/img/durham/durham_member_heil.jpg)

Marian is a PhD student in Durham working on High Energy Jets. His recent work where about simulating Higgs boson with multijets production.

##### Hitham Hassan

##### Marek Schoenherr

Members at Edinburgh
-------------------------

***

##### Jennifer M. Smillie

![](/img/durham/durham_member_smillie.png)

Jenni is an author of the High Energy Jets project. Her main research focus is addressing challenges for calculations in QCD which are posed by current and future colliders, especially the LHC. In particular she is keen to develop our understanding of the behaviour of QCD in high energy collisions, to improve the accuracy and stability of our Standard Model predictions for key processes. This is necessary to exploit the full discovery potential of the LHC and beyond.

##### Graeme Nail

![](/img/durham/durham_member_nail.png)

Graeme is a PDRA at the University of Edinburgh working on QCD and Monte Carlo event generators. He is a member of the Herwig and HEJ projects.