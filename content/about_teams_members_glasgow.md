---
title: "About"
lead: "Teams > Glasgow members"
sidebar: right
widgets: ["sidemenu_about", "team_home_pages"]
toc: false
---

Current members
---------

***

##### Andy Buckley (Team Leader)

![](/img/glasgow/glasgow_member_buckley.png)

Andy Buckley is a professor at the University of Glasgow, and an author of the
Rivet and Professor toolkits for MC analysis and tuning; the Contur, TACO,
GAMBIT and TopFitter BSM data-reinterpretation projects; and the LHAPDF, HepMC,
and various other small tools and utility libraries. His research interests are
in experimental measurement and MC modelling of QCD dynamics, in particular
heavy flavour and soft/collective QCD phenomena, and computational methods for
designing and combining BSM-sensitive measurements. Andy is a member of the
ATLAS experimental collaboration.


##### Christoph Englert

##### David Miller

##### Sophie Renner

##### David Sutherland

##### Stephen Brown

##### Neil Warrack

##### Tomasz Procter

##### Jamie Yellen

##### Benjamin Fuks (Sorbon Université and LPTHE)

Previous members
----------------

***

##### Peter Galler

##### Holger Schulz

Short-term students
-------------------

***

##### Jack Araz (Concordia/IPPP)

Jack Araz is a PhD student from Concordia University in Montreal, undertaking a short-term MCnet studentship at the University of Glasgow with Andy Buckley and Benjamin Fuks. Jack's project work is centred on effective-field theory studies of highly-boosted top quarks, probing the ability of future colliders to place statistical limits on generic Beyond Standard Model physics in the top sector. This involves incorporating the HEPTopTagger code into Rivet and MadAnalysis5, and studying how analyses using boosted tops can maximise sensitivity to EFT operators. The study will be made more realistic by use and development of fast detector-emulation systems in the two analysis frameworks, with particular emphasis on realistic modelling of jet substructure biasing and its uncertainties. Natural project extensions are EFT study of spin correlations in top-pair production, boosted tV systems, and prototyping a common analysis framework for BSM search reinterpretation.

##### Sukanya Sinha (Witwatersrand)

##### Max Knobbe (Goettingen)
