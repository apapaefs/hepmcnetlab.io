---
title: "About"
lead: "Teams > Göttingen members"
sidebar: right
widgets: ["sidemenu_about", "team_home_pages"]
toc: false
---

Team leader
---------

***

##### Steffen Schumann

![](/img/goettingen/goettingen_member_schumann.png) 

Steffen Schumann is an author of the Sherpa program. His research interests center on QCD phenomenology and in particular the development and application of perturbative methods.

Post-docs
---------

***

##### Enrico Bothmann

![](/img/goettingen/goettingen_member_bothmann.png) 

Enrico Bothmann graduated from the University of Göttingen in 2016. From 2016-2018 he as been a postdoc at the Higgs Center at the University of Edinburgh. Since fall 2018 he is back in Göttingen. Enrico is a core Sherpa developer. His particular interests range from QCD uncertainty estimations using reweighting techniques to using machine learning methods to mimick parton showers.

Enrico is a member of the MCnetITN3 student and postdoc committee.

PhD students
------------

***

##### Simon Luca Villani (MCnetITN ESR)

Simon Luca Villani is a PhD student funded by MCnetITN3. He has started his studies in April 2019, after completing his MSc studies at the University of Bologna. Simon Luca's research aims for an improved theoretical modelling of scattering processes that have their first non-vanishing contribution at the one-loop level. Examples thereof are LHC processes like Higgs-boson or vector-boson-pair production in gluon fusion.

##### Timo Janssen

##### Max Knobbe

Former members
--------------

***

##### Vincent Theeuwes

Vincent Theeuwes is a DAAD P.R.I.M.E. funded postdoctoral researcher. The first year of his fellowship he has spent with Gregory Soyey at IPhT Paris-Saclay. Vincent's research is focused on perturbative QCD and in particular analytical methods for the resummation of large kinematic logarithms.

##### Suman Deb (MCnetITN short term student)

Suman is a PhD student in experimental Physics from the Indian Institute of Technology Indore/India. His thesis work is concerned with event shape analysis with the ALICE experiment at the LHC. He currently holds an MCnetITN short term studentship with the Göttingen team. His short term project is concerned with improving the modelling of the underlying event simulations in the Sherpa generator.

##### Jeremy Baron (MCnetITN short term student)

Jeremy Baron is a PhD student in theoretical physics from the University of Buffalo/US. His thesis work in concerned with various jet-substructure methods. He joined the Göttingen team as an MCnetITN3 funded short-term student, collaborating with us on tuned comparision of the Sherpa Monte Carlo against analytical resummed predictions and phenomenological aspects of jet-substructure and event grooming techniques.

##### Diogo Buarque Franzosi

##### Daniel Reichelt (now Durham)

##### Chang Wu