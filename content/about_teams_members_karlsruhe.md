---
title: "About"
lead: "Teams > Karlsruhe members"
sidebar: right
widgets: ["sidemenu_about", "team_home_pages"]
toc: false
---

Current members
---------

***

##### Stefan Gieseke (Team Leader)

![](/img/karlsruhe/karlsruhe_member_gieseke.png) 

Stefan Gieseke is the team leader of the Karlsruhe group and one of the main authors of Herwig. His main research interest is the description of parton showers and their matching to higher order perturbative calculations. Further research is about the interplay between perturbative and non-perturbative aspects of event generation as e.g. in the underlying event.

##### Patrick Kirchgaeßer

![](/img/karlsruhe/karlsruhe_member_kirchgaesser.png) 

Patrick Kirchgaeßer is a PhD  student at KIT since March 2017. He is interested in non-perturbative aspects of particle production in Herwig.

##### Emma Simpson Dore

Emma Simpson Dore is a PhD student at KIT since October 2017.  Her main research interests are higher order contributions to parton showers. 

##### Maximilian Löschner

![](/img/karlsruhe/karlsruhe_member_loeschner.png) 

Max Löschner is a postdoc at KIT since March 2019.  Among his research interests are colour evolution in pertubative and non-perturbative contexts and higher order contributions to parton showers in general. 

Former short-term student projects
----------------------------------

***

##### Cody Duncan (Monash University)

Space time colour reconnection in Herwig: The MCnet short-term project aimed at implementing a complete description of spacetime coordinates and evolution during event generation in Herwig 7. The current version of Herwig, as with most other event generators, is formulated in an energy-momentum picture, with no concrete understanding of geometry and spacetime information generation. After a high-energy collision, outgoing QCD partons will undergo bremsstrahlung, lowering their energy more and more until reaching the non-perturbative, low-energy regime of QCD. At this energy scale and below, phenomenological models are used to attempt to describe hadronization, the process by which individual coloured partons coalesce into composite colourless hadrons. One piece of hadronization is colour reconnection, which switches around the colour connections between partons, aiming to reduce some kinematic measure of the system, which in Herwig is invariant mass.

A new model aims to generate and propagate spacetime coordinates during the contributing stages of the event, namely the multiple parton interactions and the parton shower. Multiple parton interactions occur in proton-proton collisions due to the composite nature of protons. Since protons are finitely sized objects, when they collide the constituent partons which undergo scattering processes may occur at transversely separated points in spacetime. These transverse separations can be of the size of the proton, potentially reducing the likelihood of causal connection. By using spacetime coordinates to perform colour reconnection, we reflect this causal separation. Our new model generates these transverse coordinates using the protons’ individual form factors and the overlap between them.

While showering, they are expected to travel a finite distance, analogous to the Heisenberg Uncertainty Principle. In our model, the partons with higher energy (i.e. earlier emissions) should travel a shorter distance with respect to the lower energy partons (i.e. later emissions). The showering process smears the initial transverse separation of the multiple parton interactions, allowing nearby systems to come into causal contact, and changing the geometry of the system. This setup is perfect for allowing colour reconnection to change the colour topology of the event based on the distance between individual partons or entire multiple parton interaction systems.

##### Emma Simpson Dore (Vienna)

Investigating singularities in NLO splitting kernels: The parton shower in Herwig is the product of many years of continuous development and its main purpose is to evolve the event from the hard energy scale to an infrared cutoff scale where non-perturbative hadronisation models take over. The next step in development of the shower is to include higher orders to increase the logarithmic accuracy. This requires the implementation of splitting functions for two or more emissions which involves the organisation and bookkeeping of the singularity structures of QCD cross sections. These higher order calculations are to be implemented in a methodical and algorithmic manner to allow for efficient computing. The soft and collinear limits are often treated separately however, it has been one goal of this project to look at the cross over between these two limits and the details of the relevant singularities as this includes a much wider region of phase space. The overall aim is to develop a higher order parton shower that can be implemented in Herwig and will improve the accuracy of the event simulation.



