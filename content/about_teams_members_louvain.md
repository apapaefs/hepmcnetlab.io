---
title: "About"
lead: "Teams > Louvain members"
sidebar: right
widgets: ["sidemenu_about", "team_home_pages"]
toc: false
---

Current members
---------

***

##### Fabio Maltoni (Team Leader)

![](/img/louvain/louvain_member_maltoni.png) 

Fabio Maltoni is member of the Centre for Cosmology, Particle Physics and Phenomenology (CP3) in Louvain since 2005. His research has mainly focused on Higgs boson production and decay in models with extended Higgs sectors, production and decay of quarkonia, properties and structure of QCD amplitudes, and top-quark phenomenology. Over the past decade Fabio Maltoni has devoted most of his attention to the computer simulation of collider physics and the automated computation of scattering amplitudes. He is one of the authors of MadGraph 5 that provides a platform where pheno projects can be developed, including SM and BSM physics, LO and NLO accurate predictions, such as aMC@NLO.

##### Chiara Arina

![](/img/louvain/louvain_member_arina.png)

Chiara research activities relate to the dark matter problem within a particle physics perspective. Her theoretical work focuses on dark matter model building in connection with the problem of generation of neutrino masses and/or of the visible matter. Her phenomenological research focuses on various aspects of dark matter detection (direct, indirect and at collider), with a particular attention to the statistical approach.

##### Celine Degrande

![](/img/louvain/louvain_member_degrande.jpg)

Celine is working on Effective Field Theory (EFT) at colliders mainly in top and electroweak sector. Recently, she has been focusing on the interplay between PDF and EFT fits and on the computation of NLO QCD corrections for the SMEFT. She is the author of NLOCT and one of the FeynRules authors. She is also investigating how the photon polarisation in cosmic rays can be used to probe BSM physics.

##### Jan Heisig

![](/img/louvain/louvain_member_Heisig.png)

Jan is conducting research in the fields of theoretical particle physics and cosmology investigating beyond standard models and their phenomenology at colliders, for astrophysical and cosmological observations. He is a developer of the numerical tools MadDM and SModelS.

##### Luca Mantani

![](/img/louvain/louvain_member_mantini.png)

Luca is a PhD student in Louvain since September 2017. His thesis work will focus on new physics searches at the LHC in the context of simplified models and EFT. In particular, he will consider the EFT interpretation of the LHC data, improving it by computing higher order corrections in QCD/EW and in the EFT expansion, by study possible UV completions (full or simplified) and how to relate them to the EFT, devising new observables and strategies to constrain the Wilson coefficients, to explore new techniques to more reliably evaluate the uncertainties and to perform global fits using not only LHC data but also EW precision observables and future experiments. 

##### Olivier Mattelaer

![](/img/louvain/louvain_member_mattelaer.png)

Dr. Olivier Mattelaer is research scientist in Louvain since 2009. He is the main develloper of the MadGraph5_aMC@NLO suite of program.

##### Richard Ruiz 

![](/img/louvain/louvain_member_ruiz.png)

I am a high energy physicist and collider theorist. My primary research focuses on understanding the origin of tiny, nonzero neutrino masses and tests of neutrino mass models (Seesaw models) at collider experiments, such as the Large Hadron Collider. This includes extended gauge sectors (LR-Symmetric, GUT models), scalar sectors (SU(2)L doublets and triplets), and fermionic sectors (SU(2)L singlets and triplets). I have a particular interest in collider signatures involving initial-state electroweak vector bosons (vector boson fusion/scattering) and the impact of (resummed) QCD corrections on (new physics) collider processes. This includes resummation in the context of perturbative QCD and SCET.

##### Ramon Winterhalder

![](/img/louvain/louvain_member_winterhalder.png)

Ramon has been a postdoctoral researcher in Louvain since 2021. He is working on the intersection of high-energy physics and machine learning. His research aims to establish data-driven techniques in high-energy physics and to enhance standard simulation methods with (generative) neural networks. In particular, he works on improving phase integration using normalizing flows to reduce the numerical integration error and to increase the unweighting efficiency.

##### Xiaoran Zhao

![](/img/louvain/louvain_member_zhao.png)

Xiaoran is a PhD student in Louvain since April 2017. His research mainly focuses on Higgs boson self-coupling extraction, and (multi-)loop corrections in QCD/EW and EFT. In particular, he studied single and multiple Higgs boson production at the LHC and future colliders, and focused on constraining the Higgs potential through LO contribution, as well as through loop corrections. Now he is working on improving theoretical prediction through computing higher order corrections in QCD and EW, at lepton colliders and hadron colliders.

Short-term students
-------------------

***

##### Kiran Ostrolenk

![](/img/louvain/louvain_member_ostralenk.png)

Kiran Ostrolenk is a PhD student working on NLO Matching within Herwig.

Kiran has also currently implemented helicity recycling in MadGraph. This means making sure that an external spinor or internal propagator is only calculated once at a given helicity. This is important when summing an amplitude over all possible helicities. This has resulted in a 50% speed increase for complex processes such as *gg->ttbargg* and *pp->W+W-jj*.

##### Federico Ambrogi

![](/img/louvain/louvain_member_ambrogi.png)

The numerous results of the searches for Dark Matter (DM) can be used to constrain the parameter space of many Beyond the Standard Model (BSM) theories. The focus on this Studentship was the development of the new features of MadDM, and the release of version 3.0  a plugin of the MadGraph5_aMC@NLO framework.

The most important feature of MadDM 3.0 is a dedicated module for the calculation of the theory predictions for DM annihilation, as well as the comparison with the latest results from the Fermi-LAT experiment for DM annihilation into gamma rays in dwarf spheroidal galaxies. This new module extends the capabilities of the previous versions, i.e. the computation of the relic density and the direct detection rates, making MadDM 3.0 a comprehensive tool for the study of Dark Matter phenomenology.
 
With its user friendly interface, MadDM 3.0 allows the users to choose between two running modes. The 'precision' mode is based on the full chain of Monte Carlo sample production and Fermi-LAT limits calculation, and can be used for any generic model and DM annihilation into any generic final state. The 'fast' running mode can be used only for 2 body annihilation into Standard Model channels, and the limits are extracted from the dedicated module of experimental results; despite being less general, it can be efficiently used to scan and test large parameter spaces.

The new version 3.0 was released in March 2018 together with its extended user manual (arXiv:1804.00044) which was recently accepted for publication in Physics of the Dark Universe.

##### Stefan von Buddenbrock

![](/img/louvain/louvain_member_buddenbrock.png)

Starting as a short term student in 2019, Stefan worked at CP3 on improving the computational efficiency of the Matrix Element Method for signal processing in particle physics. He finished his studentship in June 2019. In his home institute (the University of the Witwatersrand in South Africa), he is currently finishing his PhD. This is due to be finished by the end of 2019.
