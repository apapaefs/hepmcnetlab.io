---
title: "Publications"
menu:
  main:
    name: "Publications"
    weight: 7
sidebar: right
widgets: ["sidemenu_publications"]
toc: false
---

See the sidebar links for details of scientific publications written by members
of the MCnet community (and for instructions for authors to identify their work
as part of MCnet).

Or manually search for papers with MCnet report IDs on
[Inspire](https://inspirehep.net/literature?sort=mostrecent&q=report_numbers.value%3Amcnet) or
[arXiv](https://arxiv.org/search/?query=mcnet&searchtype=report_num&abstracts=show&order=-announced_date_first).

We also present [guidelines for the users of the software](/publications_guidelines) written by the MCnet members.
