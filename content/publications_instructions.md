---
title: "Instructions"
lead: "Publications"
menu:
  side_publications:
    name: "Instructions"
    weight: 101
sidebar: right
widgets: ["sidemenu_publications"]
toc: false
---


Publication instructions for MCnet members
------------------------------------------

Follow these instructions when submitting your paper to the arXiv to ensure that it appears on the MCnet website:

1. On the day you submit to the archive go to the latest year on the left.
2. Find the MCnet preprint number of the latest paper (in the list of report numbers).
3. Take the next highest preprint number and include it in the list of report numbers when you submit to the archive.

The paper should then be found automatically and added to the list.

<!--
Any work that benefitted from MCnet funding, whether by the salary of one of its
authors, by travel funds to enable collaboration, or by some other means, should
include the following sentence in its Acknowledgements: **This work has received
funding from the European Union's Horizon 2020 research and innovation programme
as part of the Marie Skłodowska-Curie Innovative Training Network MCnetITN3
(grant agreement no. 722104).**
-->
